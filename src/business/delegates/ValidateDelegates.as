package business.delegates
{
	import services.validateservice.ValidateService;

	public class ValidateDelegates
	{
		import com.adobe.cairngorm.business.ServiceLocator;
		import mx.rpc.http.HTTPService;
		import mx.rpc.IResponder;
		import mx.rpc.AsyncToken;
		
		private var __locator:ServiceLocator=ServiceLocator.getInstance();
		private var __service:ValidateService;
		private var __responder:IResponder;
		
		public function ValidateDelegates(responder:IResponder)
		{
			__service = new ValidateService;
			__responder = responder;
		}
		
		public function check(email:String):void
		{
			var token:AsyncToken = __service.IsValidEmail(email);
			token.addResponder(__responder);
		}
	}
}
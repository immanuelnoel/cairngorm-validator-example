package business.commands
{
	import business.delegates.ValidateDelegates;
	import business.events.ValidateEvent;
	
	import com.adobe.cairngorm.business.ServiceLocator;
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import model.ModelLocator;
	
	import mx.rpc.Responder;
	
	public class ValidateCommand implements ICommand
	{
		private var __locator:ServiceLocator = ServiceLocator.getInstance();
		
		public function execute(event:CairngormEvent):void
		{
			var customEvent:ValidateEvent = event as ValidateEvent;
			
			var responder:Responder = new Responder(customEvent.callbackFunction, null);
			var delegate:ValidateDelegates = new ValidateDelegates(responder);
			
			delegate.check(customEvent.data.email);
		}
	}
}
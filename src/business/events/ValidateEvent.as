package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	public class ValidateEvent extends CairngormEvent
	{
		static public var EVENT_ID:String="ValidateEvent";
		public var callbackFunction:Function;
		
		public function ValidateEvent()
		{
			super(EVENT_ID);
			data = new Object();
			callbackFunction = new Function();
		}
	}
}
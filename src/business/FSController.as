package business
{
	import business.commands.*;
	import business.events.*;
	
	import com.adobe.cairngorm.control.FrontController;

	public class FSController extends FrontController
	{
		public function FSController()
		{
			super();
			addCommand(ValidateEvent.EVENT_ID,ValidateCommand);
		}
	}
}